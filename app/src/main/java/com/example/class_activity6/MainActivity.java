package com.example.class_activity6;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;


import android.os.AsyncTask;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    HashMap<Integer,Student> students_map = new HashMap<>();
ListView student_list;
  ArrayList<HashMap<Integer,Student>> maplist = new ArrayList<>();
int index=0;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
student_list=findViewById(R.id.std_list);

        new GetStudents().execute();


    }

    private class GetStudents extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloadig",Toast.LENGTH_LONG).show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            addDataToList();
        Toast.makeText(getApplicationContext(),"Called Mehod",Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = "https://run.mocky.io/v3/ade12cda-3edb-44bb-b3e1-129166060de2";
            String jsonStr = sh.makeServiceCall(url);

            // Log.e(TAG, "Response from url: " + jsonStr);
            System.out.println(jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray students = jsonObj.getJSONArray("Students");


                    for (int i = 0; i < students.length(); i++) {
                        Student s=new Student();
                        s.setFirst_name(students.getJSONObject(i).getString("First name"));
                        s.setLast_name(students.getJSONObject(i).getString("Last name"));
                        s.setAge(students.getJSONObject(i).getString("age"));
                        s.setEnrollement_no(students.getJSONObject(i).getString("enrollment number"));
                        s.setClass_c(students.getJSONObject(i).getString("Class"));

                        students_map.put(i, s);
                    }



                } catch (final JSONException e) {
                    // Log.e(TAG, "Json parsing error: " + e.getMessage());
                    System.out.println(e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                //Log.e(TAG, "Couldn't get json from server.");
                System.out.println("Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }


    }

    class MyAdapter extends BaseAdapter
    {
Context context;
        ArrayList<HashMap<Integer, Student>> items;
        public MyAdapter(Context context, ArrayList<HashMap<Integer, Student>> items) {
            this.context = context;
            this.items = items;
        }
        @Override
        public int getCount() {
            return items.get(0).size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            LayoutInflater _inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
if(v==null)
                v = _inflater.inflate(R.layout.student_item, null);


            parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            TextView firstname = v.findViewById (R.id.first_name);
            TextView lastname = v.findViewById (R.id.last_name);
            TextView age = v.findViewById (R.id.age);
            TextView en_n = v.findViewById (R.id.en_no);
            TextView class_en = v.findViewById (R.id.student_class);

            Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_LONG).show();

            firstname.setText ("First Name: "+ maplist.get(0).get(position).getFirst_name().toUpperCase());
            lastname.setText ("Last Name: "+maplist.get(0).get(position).getLast_name().toUpperCase());
            age.setText ("Age: "+maplist.get(0).get(position).getAge().toUpperCase());
            en_n.setText ("Enroll_No: "+maplist.get(0).get(position).getEnrollement_no().toUpperCase());
            class_en.setText ("Class: "+maplist.get(0).get(position).getClass_c().toUpperCase());
            return v;
        }
    }


    private void addDataToList() {

        maplist.add(students_map);
        MyAdapter student_adapter = new MyAdapter(getApplicationContext(),maplist);

    student_list.setAdapter(student_adapter);
        ((BaseAdapter)student_list.getAdapter()).notifyDataSetChanged();
    }
}
